interface FormulaireVuetify{
    reset(): void
    resetValidation(): void
    validate(): boolean
}
export {FormulaireVuetify}